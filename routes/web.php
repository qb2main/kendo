<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TranslationController@index');
Route::get('/translations', 'TranslationController@getJson');
Route::post('/translations/create', 'TranslationController@create');
Route::post('/translations/update', 'TranslationController@update');
Route::post('/translations/destroy', 'TranslationController@destroy');
Route::get('/translations/languages_ids', 'TranslationController@getLanguagesIds');
