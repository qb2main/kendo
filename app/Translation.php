<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $primaryKey = 'translation_id';

    protected $fillable = [
        'description',
        'label_id',
        'text',
        'language_id',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language() {
        return $this->belongsTo('App\Language', 'language_id', 'language_id');
    }
}
