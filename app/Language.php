<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $primaryKey = 'language_id';

    protected $fillable = [
        'code',
        'label',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function translation() {
        return $this->hasOne('App\Translation', 'language_id', 'language_id');
    }
}
