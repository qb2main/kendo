<?php

namespace App\Http\Controllers;

use App\Language;
use App\Translation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TranslationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('kendo_test.grid');
    }

    /**
     * @return string
     */
    public function getJson() {
        $translations = Translation::all()->toJson();
        return $translations;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function create(Request $request) {
        $translations = json_decode($request->models);
        if ($translations and count($translations)) {
            foreach ($translations as $translation) {
                $new_translations[] = [
                    'description' => $translation->description,
                    'label_id' => $translation->label_id,
                    'text' => $translation->text,
                    'language_id' => $translation->language_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                ];
            }
            Translation::insert($new_translations);

            //this response is only as some one for next data using in view
            $response = [
                'answer' => "Success"
            ];
            return json_encode($response);
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function update(Request $request) {
        $up_translations = json_decode($request->models);
        if ($up_translations and count($up_translations)) {
            foreach ($up_translations as $up_translation) {
                if (isset($up_translation->translation_id)) {
                    $translation = Translation::find($up_translation->translation_id);
                    if ($translation) {
                        $translation->update([
                            'description' => $up_translation->description,
                            'label_id' => $up_translation->label_id,
                            'text' => $up_translation->text,
                            'language_id' => $up_translation->language_id,

                        ]);

                        //this response is only as some one for next data using in view
                        $response = [
                            'answer' => "Success"
                        ];
                        return json_encode($response);
                    }
                }
            }
        }
    }

    /**
     * @param Request $request
     */
    public function destroy(Request $request) {
        $up_translations = json_decode($request->models);
        if ($up_translations and count($up_translations)) {
            $ids_to_delete = [];
            foreach ($up_translations as $up_translation) {
                if (isset($up_translation->translation_id)) {
                    $ids_to_delete[] = $up_translation->translation_id;
                }
            }
        }
        if (count($ids_to_delete)) {
            Translation::whereIn('translation_id', $ids_to_delete)->delete();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getLanguagesIds() {
        $languages_ids = Language::all();
        return $languages_ids;
    }

}
