@extends('layouts.wrapper')

@section('content')
    <div>
        {{--<button id="saveChanges">Save Changes</button>--}}
        {{--<br /><br />--}}
        <div id="grid"></div>
        <br />
        <button onclick="insertItem()">+</button>
    </div>
@endsection

@section('scripts_bottom')
    <script>
        $(document).ready(function () {
                dataSource = new kendo.data.DataSource({
                    transport: {
                        read:  {
                            url: "/translations",
                            dataType: "json"
                        },
                        update: {
                            url: "/translations/update",
                            dataType: "jsonp",
                            type:"POST"
                        },
                        destroy: {
                            url: "/translations/destroy",
                            dataType: "jsonp",
                            type:"POST"
                        },
                        create: {
                            url: "/translations/create",
                            dataType: "jsonp",
                            type:"POST",

                        },
                        parameterMap: function(options, operation) {
                            if (operation !== "read" && options.models) {
                                return {models: kendo.stringify(options.models)};
                            }
                        }
                    },
                    batch: true,
                    pageSize: 20,
                    schema: {
                        model: {
                            id: "translation_id",
                            fields: {
                                translation_id: { editable:false },
                                description: {  },
                                label_id: { validation: { required: true } },
                                text: { validation: { required: true } },
                                language_id: { validation: { required: true } }
                            }
                        }
                    }
                });

            $("#grid").kendoGrid({
                dataSource: dataSource,
                navigatable: true,
                pageable: true,
                // height: 550,
                // toolbar: ["create", "save", "cancel"],
                toolbar: ["save", "cancel"],
                columns: [
                    { field: "translation_id", title: "Translation ID" },
                    { field: "description", title: "Description" },
                    { field: "label_id", title: "Label ID"},
                    { field: "text", title: "Text"},
                    { field: "language_id", title: "Language ID", editor: languageIdDropDownEditor},
                    { command: ["edit", "destroy"], title: "&nbsp;", width: 250 }],
                editable: "inline"
            });


            var users = $("#grid").data("kendoGrid").dataSource.data();

            console.log(users);

        });

        function languageIdDropDownEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataTextField: "language_id",
                    dataValueField: "language_id",
                    dataSource: {
                        type: "array",
                        transport: {
                            read:  "/translations/languages_ids"
                        }
                    }
                });
        }

        // $("#saveChanges").kendoButton({
        //     click: function(e) {
        //         var grid = $("#grid").data("kendoGrid");
        //         grid.saveChanges();
        //     }
        // });

        function insertItem(){
            var grid = $("#grid").data("kendoGrid");
            grid.addRow();
            $(".k-grid-edit-row").appendTo("#grid tbody");
        }
    </script>
@endsection