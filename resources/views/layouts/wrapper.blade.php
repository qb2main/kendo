<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ URL::asset('css/kendo.default.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/kendo.common.min.css') }}" rel="stylesheet" type="text/css">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">

        @yield('content')

    </div>
</div>

<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{ URL::asset('js/kendo.all.min.js') }}"></script>

@yield('scripts_bottom')

</body>
</html>
