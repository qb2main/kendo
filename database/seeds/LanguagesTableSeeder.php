<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            0 => [
                'code' => 'ua',
                'label' => 'ua',
            ],

            1 => [
                'code' => 'en',
                'label' => 'en',
            ],

            2 => [
                'code' => 'de',
                'label' => 'de',
            ],

        ]);

    }
}
