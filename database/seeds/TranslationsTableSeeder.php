<?php

use Illuminate\Database\Seeder;

class TranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('translations')->insert([
            0 => [
                'description' => 'save button',
                'label_id' => 'save',
                'text' => 'Зберегти',
                'language_id' => 1,
            ],

            1 => [
                'description' => 'save button',
                'label_id' => 'save',
                'text' => 'Save',
                'language_id' => 2,
            ],

            2 => [
                'description' => 'save button',
                'label_id' => 'save',
                'text' => 'Sparen',
                'language_id' => 3,
            ],

            3 => [
                'description' => 'cancel button',
                'label_id' => 'cancel',
                'text' => 'Відмінити',
                'language_id' => 1,
            ],

            4 => [
                'description' => 'cancel button',
                'label_id' => 'cancel',
                'text' => 'Cancel',
                'language_id' => 2,
            ],

            5 => [
                'description' => 'cancel button',
                'label_id' => 'cancel',
                'text' => 'Stornieren',
                'language_id' => 3,
            ],

        ]);
    }
}
